import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getUserList(){
    return localStorage.getItem("userList");
  }

  setUserList(userList){ 
    localStorage.setItem("userList",userList);
  }

  getSingleUser() {
    return localStorage.getItem("userInfo");
  }


  setSingleUser(userInfo) {
    localStorage.setItem("userInfo",userInfo);
  }
}