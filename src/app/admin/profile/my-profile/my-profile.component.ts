import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/api-service/data.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {
  public oldpassword: string;
  public newpassword: string;
  public confirmpassword: string;
  successMsg:string = "";
  hide = true;
  constructor(private router: Router,private ds: DataService) {
  
  }
    
    userData = JSON.parse(this.ds.getSingleUser()) ;

  ngOnInit() {
  }

  
  changePwd(){
    let userinfo = {
      "oldpassword": this.oldpassword,
      "newpassword": this.newpassword,
      "confirmpassword": this.confirmpassword
    };

    var localUser = this.ds.getSingleUser();

    console.log(localUser);

      const changePwd = JSON.parse(localUser) ;
 
            if(changePwd.username!='' && changePwd.password==this.oldpassword && this.newpassword==this.confirmpassword )
            {
              let userinfo = {
                "fullname": changePwd.fullname,
                "mobile": changePwd.mobile,
                "emailId": changePwd.emailId,
                "address": changePwd.address,
                "username": changePwd.username,
                "password": this.newpassword
              };

              this.ds.setSingleUser(JSON.stringify(userinfo));
              console.log(userinfo);
              this.successMsg = "Password Changed Successfully";
              this.router.navigate(['/admin/profile']);
            }          
         
    // this.router.navigate(['/admin/my-documents']);
  }
}
