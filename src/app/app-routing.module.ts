import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { RegisterComponent } from 'src/app/auth/register/register.component';
import { ForgetPasswordComponent } from 'src/app/auth/forget-password/forget-password.component';
import { MyDocumentsComponent } from 'src/app/admin/my-documents/my-documents.component';
import { MyProfileComponent } from 'src/app/admin/profile/my-profile/my-profile.component';


const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'forget-pwd',component:ForgetPasswordComponent},
  {path:'admin/my-documents',component:MyDocumentsComponent},
  {path:'admin/profile',component:MyProfileComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent,RegisterComponent,ForgetPasswordComponent,MyDocumentsComponent,MyProfileComponent];