import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { MyProfileComponent } from './admin/profile/my-profile/my-profile.component';
import { ChangePwdComponent } from './admin/profile/change-pwd/change-pwd.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatFormField } from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { MyDocumentsComponent } from './admin/my-documents/my-documents.component';
import { HeaderComponent } from './admin/header/header.component';


export const matComponents = [ MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule ]


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MyProfileComponent,
    ChangePwdComponent,
    routingComponents,
    ForgetPasswordComponent,
    MyDocumentsComponent,
    HeaderComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    matComponents,
    ReactiveFormsModule,
    FormsModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
