import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/api-service/data.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public fullname: string;
  public mobile: string;
  public emailId: string;
  public address: string;
  public username: string;
  public password: string;

  successMsg:string = "";
  
  public userList = [];
  registerForm: FormGroup;
  
  public hide = true;
  constructor(private ds: DataService,private formBuilder: FormBuilder) { 
    
  }

  ngOnInit() {
  
}

  register() {
    
        
    let userinfo = {
      "fullname": this.fullname,
      "mobile": this.mobile,
      "emailId": this.emailId,
      "address": this.address,
      "username": this.username,
      "password": this.password
    };

    var localUserList = this.ds.getUserList();
    if (localUserList != null && localUserList != 'null') {
      this.userList = JSON.parse(localUserList);
      
    }

    this.userList.push(userinfo);
    this.ds.setUserList(JSON.stringify(this.userList));
    console.log("User info" + JSON.stringify(this.userList));
    this.successMsg = "Registration Successful";
    
  }

}
