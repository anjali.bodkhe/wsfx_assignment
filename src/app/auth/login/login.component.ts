import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/api-service/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public username: string;
  public password: string;
  hide = true;
  public userList = [];
  errorMsg:string = "";
  
  constructor(private router: Router,private ds: DataService) {
    
   }

  ngOnInit() {
  }

  login(){

    let userinfo = {
      "username": this.username,
      "password": this.password
    };

    var localUserList = this.ds.getUserList();
    if (localUserList == 'null' || localUserList == null) {
      console.log('Invalid username/Password');
      this.errorMsg = "Invalid username/password.";
    }
    else{
      const userData = JSON.parse(localUserList) ;
      
              for (const key in userData) {
                if (userData.hasOwnProperty(key)) {
                  const element = userData[key];
                  if(element.username==userinfo.username && element.password==userinfo.password)
                  {    
                    let userinfo = {
                      "fullname": element.fullname,
                      "mobile": element.mobile,
                      "emailId": element.emailId,
                      "address": element.address,
                      "username": element.username,
                      "password": element.password
                    };
                    this.ds.setSingleUser(JSON.stringify(userinfo));
                    this.router.navigate(['/admin/my-documents']);
                  }          
                  else{
                    this.errorMsg = "Invalid username/password.";
                  } 
                }
              }
    }


    // this.router.navigate(['/admin/my-documents']);
  }
}
